from sys import stderr
import typer
import rich
from rich.console import Console


def print_error(msg: str):
    """Print tagged message to stderr and abort. msg supports Rich styling.

    @example print_error("Output destination exists and is not directory.")
    """

    rich.print(
        f"[bold red]Error:[/bold red] {msg}",
        file=stderr,
    )

    raise typer.Abort()


def prompt_warning(msg: str):
    """Print tagged msg to stdout and prompt user if he wants to continue. msg supports Rich styling.

    If user doesn't confirm continuing, program will abort.

    @example prompt_warning("Output destination is not empty. Files may be overwritten.")
    """

    # Capturing output to string is done via Console capture mechanism.
    # Source: https://rich.readthedocs.io/en/stable/console.html#capturing-output
    console = Console()
    with console.capture() as capture:
        console.print(
            f"[bold yellow]Warning:[/bold yellow] {msg} [bold]Continue?[/bold]"
        )

    prompt = capture.get()

    typer.confirm(prompt, abort=True)


def print_warning(msg: str):
    """Print tagged msg to stdout. msg supports Rich styling.

    @example printwarning("Output destination is not empty. Files may be overwritten.")
    """

    rich.print(f"[bold yellow]Warning:[/bold yellow] {msg}.")


def print_info(msg: str):
    """Print tagged mst to stdout. msg supports Rich styling."""

    rich.print(
        f"[bold blue]Info:[/bold blue] {msg}",
    )
