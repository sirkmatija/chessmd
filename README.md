# Chessmd

Converts markdown with some chess related goodies to static sites.

## Usage

### Create new site

```bash
chessmd site SiteTitle
```

### Create new category

```bash
cd ./SiteTitle

chessmd category CategoryTitle
```

### Create new post

```bash

cd ./content/CategoryTitle

chessmd post PostTitle
```

## Custom markdown tags

**Not implemented yet!**

### Diagram

```diagram[pgn]
1. e4 e5
```

Will be replaced by image of chessboard after moves as specified in [pgn](https://en.wikipedia.org/wiki/Portable_Game_Notation).

Tags don't need to provided, moves should be legal though.

```diagram[fen]
rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1
```

Will be replaced by image of chessboard defined by [fen](https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation).

### Game viewer

```pgn
1. e4 e5 
```

Will be replaced by interactive game viewer.

## Development tips

When running local module python will complain about relative imports
if you change directory. Fix is simple:

```bash
python -m chessmd site newsite

cd newsite

PYTHONPATH=../ python -m chessmd category newcategory
```

## Credits

Default favicon [generated](https://realfavicongenerator.net) 
from [this svg from svg repo](https://www.svgrepo.com/svg/153706/chess).

Default default image (heh): 
[Schachspiel by Lovis Corinth, 1918](https://www.nga.gov/collection/art-object-page.74081.html).