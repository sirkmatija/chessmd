from pathlib import Path


def is_dir_empty(dir: Path) -> bool:
    """True if directory is empty, else False."""

    return not any(dir.iterdir())
