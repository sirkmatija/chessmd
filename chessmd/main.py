from pathlib import Path
import typer
import pydantic

from shutil import copytree, rmtree

from .interactions import print_error

app = typer.Typer()


BASE_DIR = Path(__file__).parent
ASSETS_DIR = BASE_DIR / "assets"


@app.command()
def site(name: str):
    """Create new site."""
    destination = Path() / name
    source = ASSETS_DIR / "site"

    copytree(source, destination)


@app.command()
def category(name: str):
    """Create new category."""
    workdir = Path().resolve()

    # If workdir is named content/, we good.
    if workdir.name != "content":
        # Otherwise we may be in site directory or anywhere.
        #  We check if we are in site.

        if (workdir / "settings.json").exists() and (workdir / "content").exists:
            # We move to content.
            workdir = workdir / "content"
        else:
            # Abort.
            print_error("Cannot find content directory. Are you in site directory?")

    # Now workdir is set to content/ folder.
    source = ASSETS_DIR / "category"
    destination = workdir / name

    copytree(source, destination)


@app.command()
def post(name: str):
    """Create new post."""
    workdir = Path().resolve()

    # If workdir is named posts/, we good.
    if workdir.name != "posts":
        # Otherwise we may be in category directory or anywhere.
        # We check if we are in category.

        if workdir.parent.name == "content":
            # We move to posts.
            workdir = workdir / "posts"
        else:
            # Abort.
            print_error(
                "Post can only be created inside category. Are you in category directory?"
            )

    source = ASSETS_DIR / "post"
    destination = workdir / name

    copytree(source, destination)


class SiteSettingsJSON(pydantic.BaseModel):
    lang: str
    author: str
    image: str
    title: str
    description: str


def uncomment_json(text: str) -> str:
    """Removes // style comments from text."""
    lines = text.splitlines()

    acc: list[str] = []

    for line in lines:
        # We skip lines that are entitely comments.
        if line.strip().startswith("//") == False:
            if "//" in line:
                # So comment is in the middle.
                comment_from = line.index("//")

                acc.append(line[0:comment_from])
            else:
                acc.append(line)

    return "\n".join(acc)


def parse_settings(settings_path: Path) -> SiteSettingsJSON:
    settings_raw = settings_path.read_text()
    settings_json = uncomment_json(settings_raw)

    settings = SiteSettingsJSON.model_validate_json(settings_json)

    return settings


@app.command()
def build():
    """Build html output."""

    workdir = Path().resolve()

    # Try to load settings.
    settings_path = workdir / "settings.json"

    if not settings_path.exists():
        # Abort.
        print_error("Cannot find settings. Are you in site directory?")

    settings = parse_settings(settings_path)

    build_dir = workdir / "dist"

    if build_dir.exists():
        rmtree(build_dir)

    # Public files are copied to build_dir.
    public_dir = workdir / "public"

    copytree(public_dir, build_dir)

    # Create index page.

    # Create category pages.

    # Create tag pages.

    # Create posts pages.

    # Create about page.
